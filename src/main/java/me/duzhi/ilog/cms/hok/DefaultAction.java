package me.duzhi.ilog.cms.hok;

/**
 * @author ashang.peng@aliyun.com
 * @date 二月 17, 2017
 */

final class DefaultAction extends Action {

    @Override
    public <T> void handle(HokInvoke.Message message, HokInvoke.Inv<T> invoke, String action) {
         invoke.invoke(message);
    }
}
